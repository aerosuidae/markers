require("mod-gui")

local mod = nil

local function check_state()
	mod = global
	mod.points = mod.points or {}
	mod.dots = mod.dots or {}
end

local debug = true

local function note(msg)
	if debug then
		game.print(msg)
	end
end

local function serialize(t)
	local s = {}
	for k,v in pairs(t) do
		if type(v) == "table" then
			v = serialize(v)
		end
		s[#s+1] = tostring(k).." = "..tostring(v)
	end
	return "{ "..table.concat(s, ", ").." }"
end

local function prefixed(str, start)
	return str:sub(1, #start) == start
end

local function distance(a, b)
	local x = b.x - a.x
	local y = b.y - a.y
	return math.sqrt(x*x + y*y)
end

local function render_dot(state)
	local position = { x = state.position.x + 0.5, y = state.position.y + 0.5 }
	state.marker = rendering.draw_circle({
		color = state.colour,
		radius = 0.5,
		filled = true,
		target = position,
		surface = state.surface,
		forces = { state.force },
		draw_on_ground = true,
	})
	if state.caption then
		state.text = rendering.draw_text({
			text = { '', state.caption },
			alignment = 'center',
			color = state.caption_colour or {r = 1, g = 1, b = 1 },
			target = { x = position.x, y = position.y-0.32 },
			surface = state.surface,
			forces = { state.force },
		})
	end
end

local function remove_dot(state)
	if state.marker then
		rendering.destroy(state.marker)
		state.marker = nil
	end
	if state.text then
		rendering.destroy(state.text)
		state.text = nil
	end
end

local function gui_toggle(player)
	mod_gui.get_frame_flow(player).markers_gui.visible = not mod_gui.get_frame_flow(player).markers_gui.visible
end

local function gui_value(element)
	if element then
		if element.type == 'slider' then
			return element.slider_value
		end
		if element.type == 'checkbox' then
			return element.state
		end
	end
end

local function on_player_selected_area(event)
	check_state()

	if not prefixed(event.item, 'markers-') then
		return
	end

	local x = math.floor(event.area.left_top.x)
	local y = math.floor(event.area.left_top.y)
	local dx = math.abs(event.area.right_bottom.x - event.area.left_top.x)
	local dy = math.abs(event.area.right_bottom.y - event.area.left_top.y)

	local click = dx < 0.2 and dy < 0.2
	local player = game.players[event.player_index]

	if event.item == 'markers-dot' then
		if click then
			local p = x..","..y
			if mod.dots[p] then
				remove_dot(mod.dots[p])
				mod.dots[p] = nil
			else
				local cfg = mod_gui.get_frame_flow(player).markers_gui.markers_cfg
				local r = math.max(0, math.min(1, tonumber(gui_value(cfg.markers_r)) or 0))
				local g = math.max(0, math.min(1, tonumber(gui_value(cfg.markers_g)) or 0))
				local b = math.max(0, math.min(1, tonumber(gui_value(cfg.markers_b)) or 0))
				mod.dots[p] = {
					colour = { r = r, g = g, b = b },
					position = { x = x, y = y },
					surface = player.surface,
					force = player.force,
				}
				local state = mod.dots[p]
				if gui_value(cfg.markers_xy) then
					state.caption = x..","..y
					state.caption_colour = { r = 1, g = 1, b = 1 }
				end
				render_dot(state)
			end
		end
		return
	end
end

local function buttons(player)
	if not mod_gui.get_button_flow(player).markers_gui_toggle then
		mod_gui.get_button_flow(player).add({
			type = 'button',
			name = 'markers_gui_toggle',
			caption = { '', 'Markers' },
			style = 'mod_gui_button',
		})
		local frame = mod_gui.get_frame_flow(player).add({
			type = 'frame',
			name = 'markers_gui',
			direction = 'vertical',
			style = mod_gui.frame_style,
			caption = { '', 'Markers' },
		})
		frame.visible = false
		local tbl = frame.add({
			type = 'table',
			name = 'markers_cfg',
			direction = 'vertical',
			column_count = 2,
		})
		tbl.add({
			type = 'label',
			caption = {'', 'R'},
			style = 'caption_label',
		})
		tbl.add({
			type = 'slider',
			name = 'markers_r',
			minimum_value = 0.0,
			maximum_value = 1.0,
			value = 0.5,
			value_step = 0.1,
			discrete_slider = true,
			discrete_values = true,
		})
		tbl.add({
			type = 'label',
			caption = {'', 'G'},
			style = 'caption_label',
		})
		tbl.add({
			type = 'slider',
			name = 'markers_g',
			minimum_value = 0.0,
			maximum_value = 1.0,
			value = 0.5,
			value_step = 0.1,
			discrete_slider = true,
			discrete_values = true,
		})
		tbl.add({
			type = 'label',
			caption = {'', 'B'},
			style = 'caption_label',
		})
		tbl.add({
			type = 'slider',
			name = 'markers_b',
			minimum_value = 0.0,
			maximum_value = 1.0,
			value = 0.5,
			value_step = 0.1,
			discrete_slider = true,
			discrete_values = true,
		})
		tbl.add({
			type = 'label',
			caption = {'', 'X,Y'},
			style = 'caption_label',
		})
		tbl.add({
			type = 'checkbox',
			name = 'markers_xy',
			state = true,
		})
	end
end

local function attach_events()
	script.on_event(defines.events.on_player_selected_area, on_player_selected_area)
	script.on_event(defines.events.on_player_created, function(event)
		check_state()
		buttons(game.players[event.player_index])
	end)
	script.on_event(defines.events.on_gui_click, function(event)
		check_state()
		if event.element.name == "markers_gui_toggle" then
			gui_toggle(game.players[event.player_index])
		end
	end)
end

script.on_init(function()
	check_state()
	attach_events()
	for i=1,#game.players do
		buttons(game.players[i])
	end
end)

script.on_load(function()
	attach_events()
end)
