
local function marker(name, icon, order)
	data:extend({
		{
			type = "selection-tool",
			name = "markers-"..name.."",
			icons = icon,
			flags = {},
			subgroup = "tool",
			order = "c[z]-c[markers-"..order.."]",
			stack_size = 1,
			stackable = false,
			selection_color = {r = 0.3, g = 0.9, b = 0.3},
			alt_selection_color = {r = 0.3, g = 0.3, b = 0.9},
			selection_mode = {"any-entity","same-force"},
			alt_selection_mode = {"any-entity","same-force"},
			selection_cursor_box_type = "entity",
			alt_selection_cursor_box_type = "entity"
		},
		{
			type = "recipe",
			name = "markers-"..name.."",
			category = "crafting",
			subgroup = "tool",
			enabled = true,
			icons = icon,
			hidden = false,
			energy_required = 1.0,
			ingredients = {},
			results = {
				{ type = "item", name = "markers-"..name.."", amount = 1 },
			},
			order = "c[z]-c[markers-"..order.."]",
		},
	})
end

marker('dot', {{ icon = '__markers__/dot.png', icon_size = 32, tint = { r = 0.8, g = 0.8, b = 0.8 } }}, 'a')
